import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import * as React from "react";

import Colors from "../constants/Colors";
import useColorScheme from "../hooks/useColorScheme";
import TabOneScreen from "../screens/TabOneScreen";
import TabTwoScreen from "../screens/TabTwoScreen";
import { RootTabParamList, RootTabScreenProps } from "../types";

import { HeaderRight } from "./HeaderRight";
import { TabBarIcon } from "./TabBarIcon";

/**
 * A bottom tab navigator displays tab buttons on the bottom of the display to switch screens.
 * https://reactnavigation.org/docs/bottom-tab-navigator
 */
const BottomTab = createBottomTabNavigator<RootTabParamList>();

export function BottomTabNavigator() {
  const colorScheme = useColorScheme();

  return (
    <BottomTab.Navigator
      initialRouteName="Home"
      screenOptions={{
        tabBarActiveTintColor: Colors[colorScheme].tint,
      }}
    >
      <BottomTab.Screen
        name="Home"
        component={TabOneScreen}
        options={({ navigation }: RootTabScreenProps<"Home">) => ({
          title: "Home",
          headerTitle: "GioFlix",
          tabBarIcon: ({ color }) => <TabBarIcon name="home" color={color} />,
          headerRight: () =>
            HeaderRight({
              navigation,
              textColor: Colors[colorScheme].text,
            }),
        })}
      />
      <BottomTab.Screen
        name="MyList"
        component={TabTwoScreen}
        options={({ navigation }: RootTabScreenProps<"MyList">) => ({
          title: "Minha Lista",
          tabBarIcon: ({ color }) => (
            <TabBarIcon name="list-alt" color={color} />
          ),
          headerRight: () =>
            HeaderRight({
              navigation,
              textColor: Colors[colorScheme].text,
            }),
        })}
      />
    </BottomTab.Navigator>
  );
}
