import { FontAwesome } from "@expo/vector-icons";
import React from "react";
import { Pressable } from "react-native";
import { HeaderRightProps } from "./interfaces";

export const HeaderRight = ({ navigation, textColor }: HeaderRightProps) => (
  <Pressable
    onPress={() => navigation.navigate("Modal")}
    style={({ pressed }) => ({
      opacity: pressed ? 0.5 : 1,
    })}
  >
    <FontAwesome
      name="user-circle"
      size={25}
      color={textColor}
      style={{ marginRight: 15 }}
    />
  </Pressable>
);
