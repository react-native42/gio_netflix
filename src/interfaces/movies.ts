export interface IMovies {
  id: string;
  title: string;
  year: number;
  image: IMovieImage;
  description: string;
}

export interface IMovieImage {
  height: number;
  id: string;
  url: string;
  width: number;
}
