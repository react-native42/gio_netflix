const http = require("https");
const movies = require("./assets/api/most-popular-movies.json");
const fs = require("fs");

const countCall = 10;
const step = 5;
const startAt = (countCall - 1) * step;
const countTotal = startAt + step || movies.length;
let currentTotal = startAt;
const allFormatedMovies = [];

const getMovieById = (id) => {
  const options = {
    method: "GET",
    hostname: "imdb8.p.rapidapi.com",
    port: null,
    path: `/title/get-videos?tconst=${id}&limit=25&region=US`,
    headers: {
      "x-rapidapi-host": "imdb8.p.rapidapi.com",
      "x-rapidapi-key": "83c6de3bd1msha48ad5380268673p1bef1cjsn3e00e21bc4f8",
      useQueryString: true,
    },
  };

  const req = http.request(options, function (res) {
    const chunks = [];

    res.on("data", function (chunk) {
      chunks.push(chunk);
    });

    res.on("end", function () {
      const body = Buffer.concat(chunks);
      // console.log(body.toString());

      const parsedBody = JSON.parse(body.toString());

      const { resource } = parsedBody;
      if (!resource) {
        currentTotal++;
        return;
      }
      const { id, image, title, year, videos } = resource;
      if (!videos) {
        currentTotal++;
        return;
      }
      if (!videos.length) {
        currentTotal++;
        return;
      }
      const [{ description }] = videos;

      allFormatedMovies.push({
        id,
        title,
        year,
        image,
        description,
      });

      currentTotal++;

      if (currentTotal >= countTotal) {
        console.log(allFormatedMovies.toString());
        fs.writeFileSync(
          `movies_list0${countCall}.json`,
          JSON.stringify(allFormatedMovies)
        );
      }
    });
  });

  req.end();
};

for (let i = startAt; i < countTotal; i++) {
  getMovieById(movies[i]);
}
