const fs = require("fs");

let allMovies = [];

let readFilesCount = 0;
const totalFilesToRead = 10;

for (let i = 0; i < totalFilesToRead; i++) {
  const number = i + 1 >= 10 ? i + 1 : "0" + (i + 1);
  const fileName = `movies_list${number}.json`;

  fs.readFile(fileName, "utf8", (err, data) => {
    // console.log(data);
    const moviesList = JSON.parse(data);
    allMovies = [...allMovies, ...moviesList];

    readFilesCount++;

    if (readFilesCount >= totalFilesToRead) {
      fs.writeFileSync(`movies_lis.json`, JSON.stringify(allMovies));
    }
  });
}
